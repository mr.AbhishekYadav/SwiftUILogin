//
//  HomeView.swift
//  LoginSU
//
//  Created by Abhishek Yadav on 03/06/24.
//

import SwiftUI

struct HomeView: View {
    var username: String
    
    var body: some View {
        VStack {
            Text("Hello, \(username)!")
                .font(.largeTitle)
                .padding()
            
            // Add more home screen content here
        }
    }
}

#Preview {
    HomeView(username: "Abhishek")
}
