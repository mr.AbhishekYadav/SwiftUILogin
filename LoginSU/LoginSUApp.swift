//
//  LoginSUApp.swift
//  LoginSU
//
//  Created by Abhishek Yadav on 03/06/24.
//

import SwiftUI

@main
struct LoginSUApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
