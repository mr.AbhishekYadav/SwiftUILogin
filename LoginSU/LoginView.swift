//
//  LoginView.swift
//  LoginSU
//
//  Created by Abhishek Yadav on 03/06/24.
//

import SwiftUI

struct LoginView: View {
    @Binding var username: String
    @Binding var password: String
    @Binding var isAuthenticated: Bool
    @State private var isShowingToast: Bool = false

    var body: some View {
        VStack {
            Text("Welcome to Login Screen")
                .font(.largeTitle)
                .padding(.bottom, 100)
            
            TextField("Username", text: $username)
                .padding()
                .background(Color.gray.opacity(0.2))
                .cornerRadius(5.0)
                .padding(.bottom, 20)
            
            SecureField("Password", text: $password)
                .padding()
                .background(Color.gray.opacity(0.2))
                .cornerRadius(5.0)
                .padding(.bottom, 50)
            
            Button(action: {
                if !username.isEmpty && !password.isEmpty {
                    isAuthenticated = true
                } else {
                    // Show toast
                    isShowingToast = true
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        isShowingToast = false
                    }
                }
            }) {
                Text("LOGIN")
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .frame(width: 120, height: 30)
                    .background(Color.blue)
                    .cornerRadius(8.0)
                    .padding(.bottom, 100)
            }
            Text("© 2024 Abhishek Yadav. All rights reserved.")
                .font(.system(.callout))
                .padding(.bottom, 100)
        }
        .padding()
        .toast(isShowing: $isShowingToast, message: "All fields are mandatory.")
    }
}

//#Preview {
//LoginView(username: .constant(""), password: .constant(""), isAuthenticated: .constant(false))
//}
