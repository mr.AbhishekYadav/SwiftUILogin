//
//  File.swift
//  LoginSU
//
//  Created by Abhishek Yadav on 03/06/24.
//

import Foundation

class ListNode<T> {
    var value: T
    var next: ListNode?
    
    init(value: T, next: ListNode? = nil) {
        self.value = value
        self.next = next
    }
}

class LinkedList<T> {
    var head: ListNode<T>?
    
    // Method to reverse the linked list
    func reverse() {
        var previous: ListNode<T>? = nil
        var current = head
        var next: ListNode<T>? = nil
        
        while current != nil {
            next = current?.next
            current?.next = previous
            previous = current
            current = next
        }
        head = previous
    }
    
    // Method to check if the linked list has a cycle
    func hasCycle() -> Bool {
        var slow = head
        var fast = head
        
        while fast != nil && fast?.next != nil {
            slow = slow?.next
            fast = fast?.next?.next
            
            if slow === fast {
                return true
            }
        }
        return false
    }
    
    // Method to append a new value to the linked list
    func append(_ value: T) {
        if head == nil {
            head = ListNode(value: value)
        } else {
            var current = head
            while current?.next != nil {
                current = current?.next
            }
            current?.next = ListNode(value: value)
        }
    }
    
    // Method to print the linked list values
    func printList() {
        var current = head
        while current != nil {
            print(current!.value, terminator: " -> ")
            current = current?.next
        }
        print("nil")
    }
}
