//
//  ContentView.swift
//  LoginSU
//
//  Created by Abhishek Yadav on 03/06/24.
//

import SwiftUI

struct ContentView: View {
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var isAuthenticated: Bool = false
    
    var body: some View {
        if isAuthenticated {
            HomeView(username: username)
        } else {
            LoginView(username: $username, password: $password, isAuthenticated: $isAuthenticated)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


//====
//#Preview {
//    ContentView()
//}
//==
